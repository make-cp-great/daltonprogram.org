---
layout: default
title: Features
---

<header id="head" class="secondary"></header>

<div class="container">

    <h1>Description of the Dalton suite features</h1>

    <p>
        The Dalton suite consists of two separate executables, Dalton and LSDalton.
        The Dalton code is a powerful tool for a wide range
        of molecular properties at different levels of theory,
        whereas LSDalton is a linear-scaling
        HF and DFT code suitable for large molecular systems, now also with some CCSD capabilites.

        Any published work arising from use of one of the
        programs must acknowledge that by a proper <a href="{{ site.baseurl }}/citation/">reference</a>.

        The following list of capabilities of the Dalton and LSDalton programs should give you some
        indication of whether or not the Dalton suite is able to meet your requirements.
    </p>

    <div class="row">
        <div class="col-md-6 col-sm-6 highlight">

            <h2>Dalton</h2>

            <h3>General Features</h3>
            <ul>
                <li> Molecular gradients, dipole gradients
                <li> Molecular Hessians
                <li> First- and second-order methods for geometry optimizations
                <li> Robust second-order methods for locating transition states
                <li> Constrained geometry optimizations; bonds, angles and dihedral
                     angles can be fixed during optimizations
                <li> General numerical derivatives that automatically makes use of the
                     highest order analytical derivative available
                <li> Vibrational analysis and vibrational averaging, including anharmonic effects
                <li> HF and DFT code MPI parallel for direct Fock matrix constructions
                     and for DFT integrations over grid points
                <li> Multiscale models (SCRF, PCM, QM/MM, PE, PDE, FDE)
                <li> Most sections can exploit point-group symmetry D<sub>2h</sub> and subgroups
            </ul>

            <h3>Molecular Integrals</h3>
            <ul>
                <li> Both vectorized and parallel integral codes
                <li> Both Cartesian and spherical Gaussian basis sets can be used.
                <li> Basis sets of any angular momentum
                <li> Can handle both segmented and generally contracted basis sets
                <li> Large, easily extensible basis set library
                <li> Allows user to type in basis sets
                <li> Dalton is supported at the <a href="http://www.basissetexchange.org/">Basis Set Exchange</a>
                <li> Effective core-potentials (ECPs)
                <li> DKH2, the Douglas-Kroll-Hess second order relativistic one-electron Hamiltonian
                <li> Atomic mean-field one-electron approximation to spin-orbit integrals
                <li> Detects full point-group symmetry and exploits D<sub>2h</sub> and subgroups
                <li> Can calculate all standard one- and two-electron integrals
                <li> P-supermatrix
                <li> A large list of non-standard one- and two-electron integrals:
                <ul>
                    <li> One- and two-electron spin-orbit integrals
                    <li> Multipole integrals to arbitrary order
                    <li> Momentum and angular momentum integrals
                    <li> Dia- and paramagnetic spin-orbit integrals (DSO and PSO)
                    <li> Fermi-contact and spin-dipole integrals (FC and SD)
                    <li> and many other one-electron integrals for real or imaginary singlet or triplet operators
                </ul>
            </ul>

            <h3>Computational Models</h3>
            <ul>
                <li> RHF and high-spin ROHF (also direct and parallel)
                <li> Kohn-Sham DFT: RKS and high-spin ROKS (also direct and parallel)
                <li> For all these SCF type methods:
                <ul>
                    <li> DIIS, Roothaan, and quadratically convergent methods
                    <li> Automatic occupation of symmetries
                    <li> Either extended Huckel or core Hamiltonian starting orbitals
                </ul>
                <ul>
                    <li> Empirical dispersion corrections DFT-D2, DFT-D3 and DFT-D3BJ
                </ul>
                <li> MP2, SCS-MP2, SOS-MP2, your own scaling of MP2; MP2-srDFT
                <li> Flexible and robust quadratically convergent complete active space (CAS) or
                     restricted active space (RAS) MCSCF wave functions
                <ul>
                    <li> Configuration state functions (CSFs) for any spin multiplicity,
                         or determinant basis for any M<sub>S</sub> value
                    <li> State-specific optimization of excited states and core hole states
                </ul>
                <li> MC-srDFT with CAS or RAS: long range MCSCF combined with short range density functionals
                <li> Full CI code (also parallel)
                <li> NEVPT2 code
                <li> Many levels of coupled-cluster (CC) wave functions (see below)
                <li> Explicitly correlated R12-MP2 and R12-CC triples models
                <li> Polarizable continuum model (PCM) and self-consistent reaction field (SCRF)
                <li> Polarizable embedding (PE) and polarizable density embedding (PDE) models
            </ul>

            <h3>HF/DFT/MCSCF Molecular Properties</h3>
            <ul>
                <li> Linear (singlet/triplet), quadratic (singlet/triplet), and cubic
                     (singlet) frequency-dependent response properties and associated
                     residues of response functions
                <li> RKS- and ROKS-DFT up to quadratic response theory
                <li> Cauchy moments
                <li> Linear second-order polarization propagator approach (SOPPA)
                <li> One-, two-, and three-photon processes
                <li> Damped response using complex polarization propagator (CPP)
                <li> Magnetic properties:
                <ul>
                     <li> Gauge origin independent magnetic properties (GIAO, aka London orbitals)
                     <li> NMR properties (nuclear shieldings and
                          all contributions to nuclear spin-spin coupling constants)
                     <li> Magnetizabilites
                     <li> Relativistic corrections to nuclear shielding constants
                     <li> EPR properties (electronic g-tensor, hyperfine coupling tensor and zero-field splitting tensor)
                </ul>
                <li> Circular dichroism properties (ECD, VCD, ROA)
                <li> Static exchange (STEX) for X-ray spectroscopy
                <li> Direct dynamics
                <li> PCM and SCRF for modeling solvent effects (equilibrium and non-equilibrium solvation)
                <li> PE and PDE models to include environment effects
                <li> Frozen density embedding for DFT
            </ul>

            <h3>CC Energies and Molecular Properties</h3>
            <ul>
                <li> Ground state energies: CCS, CC2, CCSD, CCSD(T), CC3, as well as MP2.
                <li> Subsystems CC using Cholesky decomposition.
                <li> First-order properties for ground states: Dipole and quadrupole
                     moments, second moments of the electronic charge distribution and
                     electric field gradients at the nuclei, as well as scalar-relativistic
                     one-electron corrections (Darwin and mass velocity) for CCS(=SCF),
                     CC2, MP2, CCSD, CC3, and CCSD(T).
                <li> Relativistic two-electron Darwin correction to the ground state
                     for CCS, CC2, MP2, CCSD, and CCSD(T)
                <li> Excitation energies: both singlet and triplet: CCS, CC2, CCSD, CC3. Only singlet: CIS(D) and CCSDR(3).
                <li> Oscillator strengths, transition moments and strengths,
                     absorption strengths (CCS, CC2, CCSD, CC3) for linear, quadratic and cubic
                     response functions.
                <li> CVS-CC, core-valence separated CC, particularly useful for X-ray spectra
                <li> Linear, quadratic and cubic response function properties
                     (e.g. static and frequency-dependent
                     polarizability, first and second hyperpolarizability,
                     etc.) for CCS, CC2, CCSD, and CC3.
                <li> Dispersion coefficients for linear, quadratic and cubic response functions
                     (e.g. Cauchy moments, etc.) for CCS, CC2 and CCSD.
                     For CC3 dispersion coefficients for linear response function are available.
                <li> Magnetic circular dichroism (CCS, CC2, CCSD)
                <li> Two-photon transition moments between excited states (CCS, CC2, CCSD, CC3)
                <li> Frequency-dependent second-order properties of singlet excited states (CCS, CC2, CCSD, CC3)
                <li> Cavity coupled cluster self-consistent-reaction field for solvent modeling (CC2, CCSD)
                <li> PE and PDE models to include environment effects (CC2, CCSD, CCSDR(3))
                <li> Molecular gradients, and thus geometry optimization using analytical gradients (CCS, CC2, MP2, CCSD)
            </ul>

            <h3>R12 Energies and Molecular Properties</h3>
            <p>For your information:
                The primary authors of the R12 methods in Dalton,
                Wim Klopper and Christof H&auml;ttig,
                have continued the development of the R12 methods in the
                TURBOMOLE program, and some of the R12 methods provided have been
                revised and improved in TURBOMOLE. You might want to pay for
                TURBOMOLE if you are interested in the newest versions of these methods.
            </p>

            <ul>
                <li> Explicitly correlated R12-MP2 theory
                <li> Explicitly correlated R12-CC theory
            </ul>

            <h3>Available platforms</h3>
            <p>
                Dalton has been tested on the following platforms:
            </p>

            <ul>
                <li>Linux</li>
                <li>Darwin (Mac OS X)</li>
                <li>Windows-cygwin</li>
            </ul>

            <p>
                Dalton has been tested with the following compilers (not all compilers on all systems):
            </p>

            <dl>
                <dd>Tested with 32-bit and 64-bit integers with gfortran/gcc and ifort/icc compilers.
                </dd>
            </dl>

            <p>
                We expect Dalton to run correctly on other UNIX platforms and with other UNIX
                based Fortran 90 compilers, but it is not possible for the authors to verify
                this in advance. The users are encouraged to report any problems with
                installation, compilation or execution of the program to the
                <a href="http://forum.daltonprogram.org">user forum</a>.
                The authors will try to help make the code run correctly, if sufficient
                information about the system and the test calculations is provided.
            </p>

        </div>
        <div class="col-md-6 col-sm-6 highlight">

            <h2>LSDalton</h2>

            <h3>General Features</h3>
            <ul>
                <li> Efficient and linear-scaling HF and DFT code
                <li> Robust and efficient wave-function optimization procedures
                <li> First-order methods for geometry optimizations (ground and excited states)
                <li> A variety of different molecular properties
                <li> MPI/OpenMP parallel DEC-MP2 energy, density and gradient
                <li> Local orbitals
                <li> MPI/OpenMP parallel HF and DFT
                <li> MPI/OpenMP parallel CCSD
                <li> Dynamics using HF and DFT
                <li style="color:#FF0000"> No point group symmetry </col>
                <li> PCM energies, polarisabilities, first- and second-hyperpolarisabilities, and two-photon absorptions
                <li> SNOOP scheme to obtain interaction energies
                <li> Multilayer DEC (ML-DEC) scheme, for combinations of HF, RI-MP2, CCSD, and CCSD(T)
                <li> Alpha-version of open-ended response theory for geometrical and electrical properties (HF only)
            </ul>

            <h3>Molecular Integrals</h3>
            <ul>
                <li> Linear-scaling technology (integral screening, FMM, LinK)
                <li> Density-fitting and J-engine technology for the Coulomb contribution
                <li> Highly efficient XC quadrature integration
                <li> ADMM exchange energies, gradients, exciation energies and (hyper-)polarisabilities
                <li> Vectorized and hybrid MPI/OpenMP-parallel integral code
                <li> Both Cartesian and spherical Gaussian basis sets can be used.
                <li> Basis sets of any angular momentum
                <li> Segmented or generally contracted basis sets
                <li> Large, easily extensible basis set library
                <li> LSDalton is supported at the <a href="http://bse.pnl.gov/">EMSL basis set library</a>
                <li> Can calculate all standard one- and two-electron integrals
            </ul>

            <h3>SCF optimization</h3>
            <ul>
                <li> Restricted HF and DFT code
                <li> The Trilevel, Atoms or core Hamiltonian starting guesses
                <li> The Augmented Roothaan-Hall, Trust-region SCF, DIIS and Van Lenthe DIIS for robust and
                     efficient wave-function optimization
            </ul>

            <h3>HF and DFT Molecular Properties</h3>
            <ul>
                <li> Excitation energies
                <li> Polarizabilities and first hyperpolarizabilities
                <li> Standard and damped one-photon and two-photon absorption
                <li> Permanent dipole moment of ground and excited states
                <li> Regular and excited state gradients
                <li> Second hyperpolarizabilities for HF only
                <li> Nuclear-magnetic-shielding constants for HF and DFT
                <li> Magnetic-circular-dichroism parameters for HF and DFT
            </ul>

            <h3>Coupled-Cluster (MPI/OpenMP parallel)</h3>
            <ul>
                <li> RI-MP2
                <li> CCSD
                <li> Divide-Expand-Consolidate(DEC) models:
                <ul>
                    <li> DEC-MP2 energy, density and gradient
                    <li> DEC-RI-MP2 energy
                    <li> DEC-CCSD energy
                    <li> DEC-CCSD(T) energy
                </ul>
            </ul>

            <h3>Interaction energies</h3>
            <ul>
                <li> Counterpoise correction scheme
                <li> The same number of optimized parameters scheme (SNOOP)
            </ul>

            <h3>Available platforms</h3>
            <p>
                LSDalton has been tested on the following platforms:
            </p>

            <dl>
                <dt>Linux</dt>
                <dd>Tested with gfortran (versions 4.9.1 - 6.4.0) and ifort (versions 15.0.1 - 17.0.4) compilers.
                </dd>

                <dt>Darwin (Mac OS X)</dt>
                <dd>Tested with the gfortran compiler.
                </dd>

            </dl>

            <p>
                We expect LSDalton to run correctly on other UNIX platforms and with other UNIX
                based Fortran 90 compilers than gfortran and ifort but it is not possible
                for the authors to verify this in advance.  The users are encouraged to report
                any problems with installation, compilation or execution of the
                program to the <a href="http://forum.daltonprogram.org">user forum</a>.
                The authors will try to help make the code run correctly, if sufficient
                information about the system and the test calculations is provided.
            </p>

        </div>
    </div>
</div>
